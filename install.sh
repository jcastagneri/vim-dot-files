#!/bin/bash
#	Author: Joe Castagneri				Date: August 6, 2016
# Running this script installs vim by moving the dot files to the proper locations and 
# installing packages. Note that it will overwrite existing dotfiles.

# Warn user and receive permission for install

IFS=$'\n'
# Color prints
FAILED=`tput setaf 1`
SUCCESS=`tput setaf 2`
NC=`tput setaf 4`
RESET=`tput sgr0`

declare -a SUCCESSFUL_COMMAND_LIST
declare -a FAILED_COMMAND

echo "This script will overwrite existing vim dotfiles during installation."
while true; do
	read -p "Are you sure that you want to continue? (y/n) :" yn
	case $yn in
		[Yy]* ) break;;
		[Nn]* ) echo "Goodbye!"; exit;;
		* ) echo "Please answer y/es or n/o"
	esac
done

exitScript()
{
    COUNTER=0

	echo ""
	echo "${NC}Script Summary: ${RESET}"
	#cycle through all the successful commands
	for i in ${SUCCESSFUL_COMMAND_LIST[@]}
	do
		echo "${SUCCESS} SUCCESS: ${RESET}" "${SUCCESSFUL_COMMAND_LIST[COUNTER]}"
		((COUNTER+=1))
	done

	for i in ${FAILED_COMMAND[@]}
	do
		echo "${FAILED} FAILED:  ${RESET}" "${FAILED_COMMAND[0]}"
		echo ""
		echo "${FAILED}Failed Command:${RESET}"
		echo " ${FAILED_COMMAND[1]}"
		echo ""
		echo "${FAILED}Script failure. Exiting...${RESET}"
		echo ""
		exit 1
	done

	echo "${SUCCESS}Vim environment installed successfully${RESET}"
	exit 0
}

checkActionResult()
{
	echo "${NC}$2... ${RESET}"

	eval $1 # Run the command

	if [ $? -eq 0 ]
	then
		echo "${SUCCESS} SUCCESS: $2 ${RESET}"
		SUCCESSFUL_COMMAND_LIST+=($2)
	else
		echo "${FAILED} FAILED: $2 ${RESET}"
		FAILED_COMMAND+=($2)
		FAILED_COMMAND+=($1)
		exitScript
	fi
}
ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
checkActionResult 'sudo apt-get -y install vim' 'Checking vim is installed'
checkActionResult "cp $ROOT_DIR/vimrc $HOME/.vimrc" 'Copying .vimrc into home'
checkActionResult "cp -R $ROOT_DIR/vim $HOME/.vim" 'Copying .vim dir to home'
checkActionResult 'mkdir -p ~/.vim/bundle' 'Creating bundle dir'
checkActionResult 'cd' 'Changing to home directory'
checkActionResult 'git clone https://github.com/VundleVim/Vundle.vim.git $HOME/.vim/bundle/Vundle.vim' 'Cloning vundle'
checkActionResult 'vim +PluginInstall +qall' 'Installing vim plugins'
checkActionResult 'cd .vim/bundle/YouCompleteMe' 'Changing to YouCompleteMe bundle directory'
checkActionResult 'sudo apt-get -y install build-essential cmake python-dev' 'Installing clang dependencies'
checkActionResult './install.py --clang-completer' 'Installing clang completer'
echo "Successfully installed vim dot files. Remember to update .vim/.ycm_extra_conf.py"
