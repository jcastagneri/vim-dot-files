# Vim Dot File Configuration
### Joe Castagneri 

## Current Plugins
1. vim-fugitive for git support
2. YouCompleteMe with clang for autocomplete in Python and C++
3. vim-airline for a light status bar in vim
4. Vundle for package control
5. Git-gutter for git tracking

## Colorscheme
1. Monokai syntax highlighting enabled

## Misc.
1. Tabs (hard and soft) set to four spaces
2. Lines numbers included, current line number is highlighted
3. 110th column is highlighted
4. exrc and secure set for project specific implementations
5. Newline brings cursor to previous tab level
6. Window switching written to Ctrl-<vim-direction>
7. netrws directory tree set with deafualt style 3
8. Current buffer resizes larger
9. +/_ resizes buffer height
10. </> resizes buffer width
11. <C-Y> Checks C++ code for compile errors via YCM
12. <C-F> Fixes C++ errors if available

## Installation
1. `git clone git@gitlab.com:jcastagneri/vim-dot-files.git`
2. `cd vim-dot-files`
3. `./install.sh`
